# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* project_timesheet_synchro
# 
# Translators:
# Mustafa Rawi <mustafa@cubexco.com>, 2019
# Akram Alfusayal <akram_ma@hotmail.com>, 2019
# Martin Trigaux, 2019
# Osoul <baruni@osoul.ly>, 2019
# Mohammed Albasha <m.albasha.ma@gmail.com>, 2019
# Zuhair Hammadi <zuhair12@gmail.com>, 2019
# Shaima Safar <shaima.safar@open-inside.com>, 2019
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~12.4+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-12 11:29+0000\n"
"PO-Revision-Date: 2019-08-26 09:37+0000\n"
"Last-Translator: Shaima Safar <shaima.safar@open-inside.com>, 2019\n"
"Language-Team: Arabic (https://www.transifex.com/odoo/teams/41243/ar/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ar\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:170
#, python-format
msgid "&emsp;Start"
msgstr "&emsp;بدء"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:175
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:180
#, python-format
msgid "&emsp;Stop"
msgstr "&emsp;ايقاف"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:448
#, python-format
msgid "(Last sync was unsuccessful)"
msgstr "(فشلت المزامنة الأخيرة)"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:55
#, python-format
msgid "Action"
msgstr "إجراء"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:236
#, python-format
msgid "Activity created"
msgstr "تم إنشاء الإجراء"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:120
#, python-format
msgid "Add"
msgstr "إضافة"

#. module: project_timesheet_synchro
#: model:ir.model,name:project_timesheet_synchro.model_account_analytic_line
msgid "Analytic Line"
msgstr "البند التحليلي"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:26
#, python-format
msgid "Apple App Store"
msgstr "متجر آبل"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:417
#, python-format
msgid "Are you sure that you want to delete this activity?"
msgstr "هل أنت متأكد أنك تريد حذف هذا النشاط؟"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:475
#, python-format
msgid "Are you sure that you want to reset the app?"
msgstr "هل أنت متأكد أنك تريد إعادة ضبط التطبيق؟"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:52
#, python-format
msgid "Available for iPhone, Android and Chrome."
msgstr "متوفر لأجهزة أيفون و أندرويد وكروم."

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:30
#, python-format
msgid "Blazing Fast"
msgstr "ينطلق سريعًا"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:157
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:421
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:479
#, python-format
msgid "Cancel"
msgstr "إلغاء"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:463
#, python-format
msgid ""
"Connect Timesheets to Odoo to synchronize your activities across all "
"devices."
msgstr "ربط سجلات الأنشطة بأودو لمزامنة نشاطاتك عبر كافة الأجهزة."

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:104
#, python-format
msgid "Current activity"
msgstr "النشاط الجاري"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:544
#, python-format
msgid "Database"
msgstr "قاعدة البيانات"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:252
#, python-format
msgid "Default project"
msgstr "المشروع الافتراضي"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:158
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:422
#, python-format
msgid "Delete"
msgstr "حذف"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:410
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:411
#, python-format
msgid "Discard"
msgstr "تجاهل"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:492
#, python-format
msgid "Discard data"
msgstr "تجاهل البيانات"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:569
#, python-format
msgid ""
"Either you or the Odoo server is offline at the moment. Make sure you do "
"have a connection or try again later."
msgstr ""
"هناك مشكلة في اتصالك أو اتصال سيرفر أودو بالإنترنت. تأكد أنك متصل بالإنترنت "
"أو حاول مرة أخرى لاحقًا."

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:42
#, python-format
msgid "Get Things Done"
msgstr "إنجاز الأعمال"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:25
#, python-format
msgid "Google Chrome Store"
msgstr "متجر جوجل كروم"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:24
#, python-format
msgid "Google Play Store"
msgstr "متجر جوجل بلاي"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:493
#, python-format
msgid "Keep data"
msgstr "الاحتفاظ بالبيانات"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:445
#, python-format
msgid "Last sync :"
msgstr "آخر مزامنة:"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:136
#, python-format
msgid "Locked"
msgstr "مقفل"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:439
#, python-format
msgid "Logged in as"
msgstr "قمت بتسجيل الدخول كـ"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:509
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:516
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:553
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:561
#, python-format
msgid "Login"
msgstr "تسجيل الدخول"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:521
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:542
#, python-format
msgid "Login to an on premise Odoo instance"
msgstr "تسجيل الدخول لنسخة أودو على سيرفر مؤسستك"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:507
#, python-format
msgid "Login with Odoo"
msgstr "تسجيل الدخول بأودو"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:441
#, python-format
msgid "Logout"
msgstr "تسجيل الخروج"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:256
#, python-format
msgid "Minimal duration"
msgstr "المدة الدنيا"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:49
#, python-format
msgid "More info"
msgstr "مزيد من المعلومات"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:145
#, python-format
msgid "Motivation Text"
msgstr "نص تحفيزي"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:272
#, python-format
msgid "Multiple to round up all durations"
msgstr "متعددة لجمع كل المدد"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:35
#, python-format
msgid "Never lose track of what you need to do."
msgstr "لا تتوقف أبدًا عن تتبع ما تحتاج لفعله."

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:537
#, python-format
msgid "Next"
msgstr "التالي"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:300
#, python-format
msgid "Next week"
msgstr "الأسبوع القادم"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:303
#, python-format
msgid "Next week unavailable"
msgstr "الأسبوع القادم غير متاح"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:92
#, python-format
msgid "No task"
msgstr "لا يوجد مهمة"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:489
#, python-format
msgid ""
"Note that this will create new projects and tasks on your Odoo instance"
msgstr "لاحظ أن هذا سينشئ مشروعات ومهام جديدة في نسخة أودو"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:38
#, python-format
msgid "Offline support"
msgstr "دعم دون اتصال بالإنترنت"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:440
#, python-format
msgid "On server"
msgstr "على السيرفر"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:225
#, python-format
msgid ""
"Once you have created or synchronized projects and tasks, they will appear "
"here. This will allow you to plan your day in advance."
msgstr ""
"بمجرد إنشاء أو مزامنة المشروعات والمهام، سوف تظهر هنا. سيسمح لك هذا بوضع خطة"
" يومك مسبقًا."

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:16
#, python-format
msgid "Open the App"
msgstr "فتح التطبيق"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:513
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:557
#, python-format
msgid "Password"
msgstr "كلمة المرور"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:43
#, python-format
msgid "Plan your day. Focus on important tasks."
msgstr "خطط ليومك. وركز على المهام الهامة."

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:533
#, python-format
msgid "Please enter a database name:"
msgstr "برجاء إدخال اسم قاعدة بيانات:"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:249
#, python-format
msgid "Please enter a valid duration"
msgstr "برجاء إدخال مدة صالحة"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:358
#, python-format
msgid "Please select a project first"
msgstr "الرجاء اختيار المشروع أولًا"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:340
#, python-format
msgid "Please select a project."
msgstr "الرجاء اختيار المشروع."

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:288
#, python-format
msgid "Previous week"
msgstr "الأسبوع الماضي"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:291
#, python-format
msgid "Previous week unavailable"
msgstr "الأسبوع الماضي غير متاح"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:348
#, python-format
msgid "Project"
msgstr "المشروع"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:480
#, python-format
msgid "RESET"
msgstr "إعادة تعيين"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:501
#, python-format
msgid "Reset"
msgstr "إعادة تعيين"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:116
#, python-format
msgid "Run"
msgstr "تشغيل"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:409
#, python-format
msgid "Save"
msgstr "حفظ"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:577
#, python-format
msgid "Select your Odoo instance from the list below :"
msgstr "حدد نسختك من أودو من القائمة أدناه:"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:38
#, python-format
msgid "Settings"
msgstr "الإعدادات"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:559
#, python-format
msgid "Show password"
msgstr "إظهار كلمة المرور"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:467
#, python-format
msgid "Sign In"
msgstr "تسجيل الدخول"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:468
#, python-format
msgid "Sign Up"
msgstr "التسجيل"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:31
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:34
#, python-format
msgid "Statistics"
msgstr "الإحصائيات"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:98
#, python-format
msgid "Subtract"
msgstr "طرح"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:443
#, python-format
msgid "Sync Now"
msgstr "المزامنة الآن"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:53
#, python-format
msgid "Sync in progress"
msgstr "المزامنة قيد التنفيذ"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:35
#, python-format
msgid "Synchronize"
msgstr "المزامنة"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:587
#, python-format
msgid "Syncing data, this shouldn't take long"
msgstr "مزامنة البيانات، لا ينبغي أن يستغرق هذا وقتًا طويلًا"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:353
#, python-format
msgid "Task"
msgstr "المهمة"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:455
#, python-format
msgid ""
"The server you connected to does not support timesheet synchronization. You "
"should contact your administrator in order to install the module "
"''Synchronization with the external timesheet application'' (available in "
"Odoo Enterprise Edition)."
msgstr ""
"السيرفر الذي اتصلت به لا يدعم مزامنة سجل النشاط. عليك التواصل مع مشرفك "
"لتثبيت موديول \"المزامنة مع تطبيق سجل النشاط الخارجي\" (متاح في نسخة أودو "
"المدفوعة)."

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:24
#, python-format
msgid "This Week"
msgstr "هذا الأسبوع"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:386
#, python-format
msgid "Time spent (hh:mm)"
msgstr "الوقت الذي يقضيه (hh:mm)"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:363
#, python-format
msgid "Time spent (hhmm)"
msgstr "الوقت المستغرق (hhmm)"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:265
#, python-format
msgid "Time unit"
msgstr "وحدة الزمن"

#. module: project_timesheet_synchro
#: model:ir.actions.client,name:project_timesheet_synchro.project_timesheet_synchro_app_action
#: model:ir.ui.menu,name:project_timesheet_synchro.menu_timesheet_app
msgid "Timesheet App"
msgstr "تطبيق سجل النشاط"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:21
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:233
#, python-format
msgid "Today"
msgstr "اليوم"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:28
#, python-format
msgid "Today's Plan"
msgstr "خطة اليوم"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:49
#, python-format
msgid "Toggle action"
msgstr "تبديل الإجراء"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:322
#, python-format
msgid "Total :"
msgstr "الإجمالي:"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:464
#, python-format
msgid ""
"Use Odoo to organize projects and tasks, forecast resources and invoice time"
" spent on tasks"
msgstr ""
"استخدم أودو لتنظيم المشروعات والمهمات، والتنبؤ بالمصادر وفوترة وقت تنفيذ "
"المهام"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:500
#, python-format
msgid "Use an Odoo.com account"
msgstr "استخدام حساب أودو"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:22
#, python-format
msgid "Use timesheet apps to manage your timesheets."
msgstr "استخدام تطبيقات سجل النشاط لإدارة سجلات أنشطتك."

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:295
#, python-format
msgid "Week"
msgstr "الأسبوع"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:398
#, python-format
msgid "Work Summary"
msgstr "ملخص العمل"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:39
#, python-format
msgid "Work anywhere, anytime."
msgstr "العمل في أي مكان وفي أي وقت."

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:31
#, python-format
msgid "Work in disconnected mode and sync in the background."
msgstr "العمل في وضع قطع الاتصال والمزامنة في الخلفية."

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:488
#, python-format
msgid ""
"Would you like the activities, projects and tasks that you created as a "
"guest user to be synchronized as well?"
msgstr ""
"هل تريد مزامنة الأنشطة والمشروعات والمهام التي قمت بإنشائها كمستخدم ضيف "
"أيضًا؟"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:153
#, python-format
msgid "Would you like to delete this activity?"
msgstr "هل ترغب في حذف هذا النشاط؟"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:586
#, python-format
msgid "You are logged in !"
msgstr "لقد سجلت دخولك!"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:15
#, python-format
msgid "You can try the app in a new tab:"
msgstr "يمكنك تجربة التطبيق في علامة تبويب جديدة:"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:573
#, python-format
msgid ""
"You should have received an email with a link to activate your account. Once"
" it is activated, you'll be able to"
msgstr "ينبغي أن تصلك رسالة برابط لتفعيل حسابك. بمجرد تفعيله، ستتمكن من"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:523
#, python-format
msgid "Your Odoo Server Address"
msgstr "عنوان سيرفر أودو"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:572
#, python-format
msgid "Your database and account have been created !"
msgstr "تم إنشاء قاعدة بياناتك وحسابك!"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/timesheet_app_backend_template.xml:21
#, python-format
msgid "Your personal timesheets"
msgstr "سجلات أنشطتك الشخصية"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:554
#, python-format
msgid "ex: admin@example.com"
msgstr "مثال: admin@example.com"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:525
#, python-format
msgid "http://"
msgstr "http://"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:526
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:527
#, python-format
msgid "https://"
msgstr "https://"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:260
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:269
#, python-format
msgid "minutes"
msgstr "الدقائق"

#. module: project_timesheet_synchro
#. openerp-web
#: code:addons/project_timesheet_synchro/static/src/xml/project_timesheet.xml:573
#, python-format
msgid "sign in"
msgstr "تسجيل الدخول"
