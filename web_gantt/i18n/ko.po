# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* web_gantt
# 
# Translators:
# JH CHOI <hwangtog@gmail.com>, 2019
# Link Up링크업 <linkup.way@gmail.com>, 2019
# Linkup <link-up@naver.com>, 2019
# Seongseok Shin <shinss61@hotmail.com>, 2019
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 13.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-07 07:21+0000\n"
"PO-Revision-Date: 2019-08-26 09:38+0000\n"
"Last-Translator: Seongseok Shin <shinss61@hotmail.com>, 2019\n"
"Language-Team: Korean (https://www.transifex.com/odoo/teams/41243/ko/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ko\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/xml/web_gantt.xml:0
#, python-format
msgid "Add"
msgstr "추가"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/xml/web_gantt.xml:0
#, python-format
msgid "Add record"
msgstr ""

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_controller.js:0
#, python-format
msgid "Are you sure to delete this record?"
msgstr ""

#. module: web_gantt
#: model:ir.model,name:web_gantt.model_base
msgid "Base"
msgstr "기본"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/xml/web_gantt.xml:0
#, python-format
msgid "Collapse rows"
msgstr ""

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_controller.js:0
#: code:addons/web_gantt/static/src/xml/web_gantt.xml:0
#, python-format
msgid "Create"
msgstr "신규"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_view.js:0
#, python-format
msgid "Day"
msgstr "일"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/xml/web_gantt.xml:0
#, python-format
msgid "Expand rows"
msgstr ""

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_view.js:0
#, python-format
msgid "Gantt"
msgstr "간트"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_view.js:0
#, python-format
msgid "Gantt View"
msgstr "간트 차트 화면"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_model.js:0
#, python-format
msgid "Grouping by date is not supported, ignoring it"
msgstr ""

#. module: web_gantt
#: code:addons/web_gantt/models/models.py:0
#, python-format
msgid "Insufficient fields for Gantt View!"
msgstr ""

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_model.js:0
#, python-format
msgid "Invalid group by"
msgstr ""

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_view.js:0
#, python-format
msgid "Month"
msgstr "월"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/xml/web_gantt.xml:0
#, python-format
msgid "Name:"
msgstr "이름 :"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/xml/web_gantt.xml:0
#, python-format
msgid "Next"
msgstr "다음"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_controller.js:0
#, python-format
msgid "Open"
msgstr "미결"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_controller.js:0
#, python-format
msgid "Plan"
msgstr "계획"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/xml/web_gantt.xml:0
#, python-format
msgid "Plan existing"
msgstr ""

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/xml/web_gantt.xml:0
#, python-format
msgid "Previous"
msgstr "이전"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/xml/web_gantt.xml:0
#, python-format
msgid "Start:"
msgstr ""

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/xml/web_gantt.xml:0
#, python-format
msgid "Stop:"
msgstr ""

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/xml/web_gantt.xml:0
#, python-format
msgid "Today"
msgstr "오늘"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_model.js:0
#, python-format
msgid "Undefined %s"
msgstr ""

#. module: web_gantt
#: model:ir.model,name:web_gantt.model_ir_ui_view
msgid "View"
msgstr "보기"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_view.js:0
#, python-format
msgid "Week"
msgstr "주"

#. module: web_gantt
#. openerp-web
#: code:addons/web_gantt/static/src/js/gantt_view.js:0
#, python-format
msgid "Year"
msgstr "년"
