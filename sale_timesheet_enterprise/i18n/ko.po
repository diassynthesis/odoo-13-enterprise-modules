# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* sale_timesheet_enterprise
# 
# Translators:
# Martin Trigaux, 2019
# JH CHOI <hwangtog@gmail.com>, 2019
# Link Up링크업 <linkup.way@gmail.com>, 2019
# Linkup <link-up@naver.com>, 2019
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~12.5+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-23 11:39+0000\n"
"PO-Revision-Date: 2019-08-26 09:38+0000\n"
"Last-Translator: Linkup <link-up@naver.com>, 2019\n"
"Language-Team: Korean (https://www.transifex.com/odoo/teams/41243/ko/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ko\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_view_kanban
msgid "<i class=\"fa fa-pause text-warning\" title=\"Timer is Paused\"/>"
msgstr ""

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_view_kanban
msgid "<i class=\"fa fa-play text-success\" title=\"Timer is Running\"/>"
msgstr ""

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.res_config_settings_view_form
msgid "<span class=\"o_form_label\">Invoicing Policy</span>"
msgstr ""

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.res_config_settings_view_form
msgid "<strong>Round timesheets</strong>"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields.selection,name:sale_timesheet_enterprise.selection__res_config_settings__invoiced_timesheet__all
msgid "All recorded timesheets"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model,name:sale_timesheet_enterprise.model_account_analytic_line
msgid "Analytic Line"
msgstr "분석 명세"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields.selection,name:sale_timesheet_enterprise.selection__res_config_settings__invoiced_timesheet__approved
msgid "Approved timesheets only"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_project__allow_billable
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task__allow_billable
msgid "Bill from Tasks"
msgstr ""

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_create_sale_order_view_form
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_create_timesheet_view_form
msgid "Cancel"
msgstr "취소"

#. module: sale_timesheet_enterprise
#: model:ir.model,name:sale_timesheet_enterprise.model_res_config_settings
msgid "Config Settings"
msgstr "설정"

#. module: sale_timesheet_enterprise
#: code:addons/sale_timesheet_enterprise/models/project_task.py:0
#, python-format
msgid "Confirm Time Spent"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model,name:sale_timesheet_enterprise.model_project_task_create_sale_order
msgid "Create SO from task"
msgstr ""

#. module: sale_timesheet_enterprise
#: code:addons/sale_timesheet_enterprise/models/project_task.py:0
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_create_sale_order_view_form
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_view_form
#, python-format
msgid "Create Sales Order"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model,name:sale_timesheet_enterprise.model_project_task_create_timesheet
msgid "Create Timesheet from task"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.actions.act_window,name:sale_timesheet_enterprise.project_task_action_multi_create_sale_order
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_create_sale_order_view_form
msgid "Create a Sales Order"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_sale_order__create_uid
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_timesheet__create_uid
msgid "Created by"
msgstr "작성인"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_sale_order__create_date
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_timesheet__create_date
msgid "Created on"
msgstr "작성일"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_sale_order__currency_id
msgid "Currency"
msgstr "통화"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_sale_order__partner_id
msgid "Customer"
msgstr "고객"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,help:sale_timesheet_enterprise.field_project_task_create_sale_order__partner_id
msgid "Customer of the sales order"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_timesheet__description
msgid "Description"
msgstr "설명"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_sale_order__display_name
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_timesheet__display_name
msgid "Display Name"
msgstr "표시 이름"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task__display_timesheet_timer
msgid "Display Timesheet Time"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_sale_order__id
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_timesheet__id
msgid "ID"
msgstr "ID"

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_view_form_inherit
msgid "Invoice your time and material from tasks"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model,name:sale_timesheet_enterprise.model_account_move_line
msgid "Journal Item"
msgstr "분개 항목"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_sale_order____last_update
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_timesheet____last_update
msgid "Last Modified on"
msgstr "최근 수정일"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_sale_order__write_uid
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_timesheet__write_uid
msgid "Last Updated by"
msgstr "최근 갱신한 사람"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_sale_order__write_date
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_timesheet__write_date
msgid "Last Updated on"
msgstr "최근 갱신일"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_res_config_settings__timesheet_min_duration
msgid "Minimal duration"
msgstr "최소 지속시간"

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_view_form
msgid "Pause"
msgstr "일시 정지"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,help:sale_timesheet_enterprise.field_project_task_create_sale_order__product_id
msgid ""
"Product of the sales order item. Must be a service invoiced based on "
"timesheets on tasks. The existing timesheet will be linked to this product."
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model,name:sale_timesheet_enterprise.model_project_project
msgid "Project"
msgstr "프로젝트 관리"

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.res_config_settings_view_form
msgid "Record time spent and invoice it based on:"
msgstr ""

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_view_form
msgid "Resume"
msgstr "이력서"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_res_config_settings__timesheet_rounding
msgid "Rounding up"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model,name:sale_timesheet_enterprise.model_sale_order_line
msgid "Sales Order Line"
msgstr "판매 주문 명세"

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_create_timesheet_view_form
msgid "Save"
msgstr "저장"

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_create_timesheet_view_form
msgid "Save time"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_sale_order__product_id
msgid "Service"
msgstr "차량 관리"

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_view_form
msgid "Start"
msgstr "시작"

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_view_form
msgid "Stop"
msgstr "중지"

#. module: sale_timesheet_enterprise
#: model:ir.model,name:sale_timesheet_enterprise.model_project_task
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_sale_order__task_id
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_timesheet__task_id
msgid "Task"
msgstr "작업"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,help:sale_timesheet_enterprise.field_project_task_create_sale_order__task_id
#: model:ir.model.fields,help:sale_timesheet_enterprise.field_project_task_create_timesheet__task_id
msgid "Task for which we are creating a sales order"
msgstr ""

#. module: sale_timesheet_enterprise
#: code:addons/sale_timesheet_enterprise/wizard/project_task_create_sale_order.py:0
#, python-format
msgid ""
"The sales order cannot be created because some timesheets of this task are "
"already linked to another sales order."
msgstr ""

#. module: sale_timesheet_enterprise
#: code:addons/sale_timesheet_enterprise/wizard/project_task_create_sale_order.py:0
#, python-format
msgid "The task is already billable."
msgstr ""

#. module: sale_timesheet_enterprise
#: code:addons/sale_timesheet_enterprise/wizard/project_task_create_sale_order.py:0
#, python-format
msgid "The task is already linked to a sales order item."
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.constraint,message:sale_timesheet_enterprise.constraint_project_project_timer_only_when_timesheet
msgid ""
"The timesheet timer can only be activated on project allowing timesheet."
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.constraint,message:sale_timesheet_enterprise.constraint_project_task_create_timesheet_time_positive
msgid "The timesheet's time must be positive"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_timesheet__time_spent
msgid "Time"
msgstr "시간"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_project__allow_timesheet_timer
msgid "Timesheet Timer"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task__timesheet_timer_first_start
msgid "Timesheet Timer First Use"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task__timesheet_timer_pause
msgid "Timesheet Timer Last Pause"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task__timesheet_timer_last_stop
msgid "Timesheet Timer Last Use"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task__timesheet_timer_start
msgid "Timesheet Timer Start"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_res_config_settings__invoiced_timesheet
msgid "Timesheets Invoicing"
msgstr ""

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_view_form_inherit
msgid "Track your time using a timer"
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,field_description:sale_timesheet_enterprise.field_project_task_create_sale_order__price_unit
msgid "Unit Price"
msgstr "단가"

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,help:sale_timesheet_enterprise.field_project_task_create_sale_order__price_unit
msgid "Unit price of the sales order item."
msgstr ""

#. module: sale_timesheet_enterprise
#: model:ir.model.fields,help:sale_timesheet_enterprise.field_project_project__allow_timesheet_timer
msgid "Use a timer to record timesheets on tasks"
msgstr ""

#. module: sale_timesheet_enterprise
#: code:addons/sale_timesheet_enterprise/wizard/project_task_create_sale_order.py:0
#: code:addons/sale_timesheet_enterprise/wizard/project_task_create_timesheet.py:0
#, python-format
msgid "You can only apply this action from a task."
msgstr ""

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.project_task_create_timesheet_view_form
msgid "hours"
msgstr "시간"

#. module: sale_timesheet_enterprise
#: model_terms:ir.ui.view,arch_db:sale_timesheet_enterprise.res_config_settings_view_form
msgid "minutes"
msgstr "분"
