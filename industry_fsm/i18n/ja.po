# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* industry_fsm
# 
# Translators:
# Martin Trigaux, 2019
# Manami Hashi <manami@roomsfor.hk>, 2019
# NOKA Shigekazu <shigekazu.noka@gmail.com>, 2019
# Norimichi Sugimoto <norimichi.sugimoto@tls-ltd.co.jp>, 2019
# Yoshi Tashiro <tashiro@roomsfor.hk>, 2019
# Tim Siu Lai <tl@roomsfor.hk>, 2019
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 13.0+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-12-05 09:37+0000\n"
"PO-Revision-Date: 2019-08-26 09:36+0000\n"
"Last-Translator: Tim Siu Lai <tl@roomsfor.hk>, 2019\n"
"Language-Team: Japanese (https://www.transifex.com/odoo/teams/41243/ja/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ja\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__delay_endings_days
msgid "# Days to Deadline"
msgstr "締切期限日数"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__working_days_open
msgid "# Working Days to Assign"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__working_days_close
msgid "# Working Days to Close"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__nbr
msgid "# of Tasks"
msgstr "タスク数"

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid "<b>Click the save button</b> to save the time spent."
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
msgid ""
"<i class=\"fa fa-long-arrow-right mx-2\" aria-label=\"Arrow icon\" "
"title=\"Arrow\"/>"
msgstr ""

#. module: industry_fsm
#: code:addons/industry_fsm/models/project.py:0
#, python-format
msgid ""
"<p class=\"o_view_nocontent_smiling_face\">\n"
"                            Create a new product\n"
"                        </p><p>\n"
"                            You must define a product for everything you sell or purchase,\n"
"                            whether it's a storable product, a consumable or a service.\n"
"                        </p>"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
msgid "<span class=\"align-middle\">for this employee at the same time.</span>"
msgstr ""

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid "Add a product by clicking on it."
msgstr ""

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid "Add a task title. <br/><i>(e.g. Boiler replacement)</i>"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
msgid "Add materials"
msgstr ""

#. module: industry_fsm
#: model:res.groups,name:industry_fsm.group_fsm_manager
msgid "Administrator"
msgstr "管理者"

#. module: industry_fsm
#: model:ir.actions.act_window,name:industry_fsm.project_task_action_all_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_all_tasks_root
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_all_tasks_todo
msgid "All Tasks"
msgstr "タスク"

#. module: industry_fsm
#: model_terms:ir.actions.act_window,help:industry_fsm.project_task_action_to_invoice_fsm
msgid "All tasks were invoiced"
msgstr ""

#. module: industry_fsm
#: model:ir.model.constraint,message:industry_fsm.constraint_project_project_fsm_imply_task_rate
msgid "An FSM project must be billed at task rate."
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "Archived"
msgstr "アーカイブ済"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__date_assign
msgid "Assignation Date"
msgstr "割当日"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__user_id
msgid "Assigned To"
msgstr "担当者"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_task__user_id
msgid "Assigned to"
msgstr "担当者"

#. module: industry_fsm
#: model:ir.ui.menu,name:industry_fsm.project_task_menu_planning_by_project_fsm
msgid "By Project"
msgstr "プロジェクト別"

#. module: industry_fsm
#: model:ir.ui.menu,name:industry_fsm.project_task_menu_planning_by_user_fsm
msgid "By User"
msgstr "ユーザ別"

#. module: industry_fsm
#: code:addons/industry_fsm/models/project.py:0
#, python-format
msgid "Choose Products"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.actions.act_window,help:industry_fsm.project_task_action_to_schedule_fsm
msgid "Click Create to start a new task"
msgstr ""

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid "Click on create invoice."
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__company_id
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "Company"
msgstr "会社"

#. module: industry_fsm
#: model:ir.model,name:industry_fsm.model_res_config_settings
msgid "Config Settings"
msgstr ""

#. module: industry_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_settings
msgid "Configuration"
msgstr "設定"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
msgid "Create Invoice"
msgstr "請求書作成"

#. module: industry_fsm
#: model_terms:ir.actions.act_window,help:industry_fsm.project_task_action_all_fsm
msgid "Create a new task"
msgstr ""

#. module: industry_fsm
#: model:ir.actions.server,name:industry_fsm.project_task_server_action_batch_invoice_fsm
msgid "Create invoice"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_view_form_inherit
msgid "Create new quotations directly from tasks"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.res_config_settings_view_form
#: model:res.groups,name:industry_fsm.group_fsm_quotation_from_task
msgid "Create new quotations directly from the tasks"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_task__currency_id
msgid "Currency"
msgstr "通貨"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__partner_id
msgid "Customer"
msgstr "顧客"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
msgid "Date"
msgstr "日付"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__date_deadline
msgid "Deadline"
msgstr "期日"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__display_name
msgid "Display Name"
msgstr "表示名"

#. module: industry_fsm
#: model:ir.model.fields,help:industry_fsm.field_project_project__is_fsm
#: model:ir.model.fields,help:industry_fsm.field_project_task__is_fsm
msgid ""
"Display tasks in the Field Service module and allow planning with start/end "
"dates."
msgstr ""

#. module: industry_fsm
#: model:project.task.type,name:industry_fsm.planning_project_stage_1
msgid "Done"
msgstr "完了"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__hours_effective
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
msgid "Effective Hours"
msgstr "有効時間数"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__date_end
msgid "Ending Date"
msgstr "終了日"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_project__allow_quotations
#: model:ir.model.fields,field_description:industry_fsm.field_project_task__allow_quotations
#: model:ir.model.fields,field_description:industry_fsm.field_res_config_settings__group_industry_fsm_quotations
msgid "Extra Quotations"
msgstr ""

#. module: industry_fsm
#: model:ir.model,name:industry_fsm.model_report_project_task_user_fsm
msgid "FSM Tasks Analysis"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_project__is_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_task__is_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_root
#: model_terms:ir.ui.view,arch_db:industry_fsm.res_config_settings_view_form
#: model:product.product,name:industry_fsm.field_service_product
#: model:product.template,name:industry_fsm.field_service_product_product_template
msgid "Field Service"
msgstr "フィールドサービス"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "Future"
msgstr "将来"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "Group By"
msgstr "グループ化"

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid ""
"Here is the <b>Field Service app</b>. Click on the icon to start managing "
"your onsite interventions."
msgstr ""

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid ""
"Here is the view of the users who are on the field. Click CREATE to start "
"your first task."
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__id
msgid "ID"
msgstr "ID"

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid ""
"If everything looks good to you, mark the task as done. When doing so, your "
"stock will automatically be updated and your task will move to te next "
"stage."
msgstr ""

#. module: industry_fsm
#: model:project.task,legend_normal:industry_fsm.planning_task_0
#: model:project.task,legend_normal:industry_fsm.planning_task_1
#: model:project.task,legend_normal:industry_fsm.planning_task_10
#: model:project.task,legend_normal:industry_fsm.planning_task_11
#: model:project.task,legend_normal:industry_fsm.planning_task_2
#: model:project.task,legend_normal:industry_fsm.planning_task_3
#: model:project.task,legend_normal:industry_fsm.planning_task_4
#: model:project.task,legend_normal:industry_fsm.planning_task_5
#: model:project.task,legend_normal:industry_fsm.planning_task_6
#: model:project.task,legend_normal:industry_fsm.planning_task_7
#: model:project.task,legend_normal:industry_fsm.planning_task_8
#: model:project.task,legend_normal:industry_fsm.planning_task_9
#: model:project.task.type,legend_normal:industry_fsm.planning_project_stage_0
#: model:project.task.type,legend_normal:industry_fsm.planning_project_stage_1
msgid "In Progress"
msgstr "進行中"

#. module: industry_fsm
#: code:addons/industry_fsm/models/project.py:0
#, python-format
msgid "Invoice"
msgstr "請求書"

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid "Invoice your Time and Material to your customer."
msgstr ""

#. module: industry_fsm
#: code:addons/industry_fsm/models/project.py:0
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
#, python-format
msgid "Invoices"
msgstr "請求書"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__state
msgid "Kanban State"
msgstr "かんばん状態"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm____last_update
msgid "Last Modified on"
msgstr "最終更新日"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__date_last_stage_update
msgid "Last Stage Update"
msgstr "最終ステージ更新日"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
msgid "Launch the Timer"
msgstr ""

#. module: industry_fsm
#: model:ir.actions.act_window,name:industry_fsm.project_task_action_fsm_map
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_tasks_map
msgid "Map"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
msgid "Mark as done"
msgstr "完了とする"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_task__material_line_product_count
msgid "Material Line Product Count"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_task__material_line_total_price
msgid "Material Line Total Price"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_product_product__fsm_quantity
msgid "Material Quantity"
msgstr ""

#. module: industry_fsm
#: model:ir.actions.act_window,name:industry_fsm.project_task_action_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_tasks_menu
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "My Tasks"
msgstr "自分のタスク"

#. module: industry_fsm
#: model:project.task.type,name:industry_fsm.planning_project_stage_0
msgid "New"
msgstr "新規"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form_fsm_quotation
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
msgid "New Quotation"
msgstr "新規見積"

#. module: industry_fsm
#: model_terms:ir.actions.act_window,help:industry_fsm.project_task_action_fsm
msgid "No future Tasks planned"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.actions.act_window,help:industry_fsm.project_task_action_to_invoice_fsm
msgid "No task to Invoice"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.actions.act_window,help:industry_fsm.project_task_action_to_schedule_fsm
msgid "No task to schedule"
msgstr ""

#. module: industry_fsm
#: model:project.task,legend_blocked:industry_fsm.planning_task_0
#: model:project.task,legend_blocked:industry_fsm.planning_task_1
#: model:project.task,legend_blocked:industry_fsm.planning_task_10
#: model:project.task,legend_blocked:industry_fsm.planning_task_11
#: model:project.task,legend_blocked:industry_fsm.planning_task_2
#: model:project.task,legend_blocked:industry_fsm.planning_task_3
#: model:project.task,legend_blocked:industry_fsm.planning_task_4
#: model:project.task,legend_blocked:industry_fsm.planning_task_5
#: model:project.task,legend_blocked:industry_fsm.planning_task_6
#: model:project.task,legend_blocked:industry_fsm.planning_task_7
#: model:project.task,legend_blocked:industry_fsm.planning_task_8
#: model:project.task,legend_blocked:industry_fsm.planning_task_9
#: model:project.task.type,legend_blocked:industry_fsm.planning_project_stage_0
#: model:project.task.type,legend_blocked:industry_fsm.planning_project_stage_1
msgid "Not validated"
msgstr "未検証"

#. module: industry_fsm
#: model:ir.model.fields,help:industry_fsm.field_report_project_task_user_fsm__working_days_open
msgid "Number of Working Days to Open the task"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,help:industry_fsm.field_report_project_task_user_fsm__working_days_close
msgid "Number of Working Days to close the task"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_task__invoice_count
msgid "Number of invoices"
msgstr ""

#. module: industry_fsm
#: code:addons/industry_fsm/models/project.py:0
#, python-format
msgid "Overlapping Tasks"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
msgid "Pause"
msgstr "一時停止"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__hours_planned
msgid "Planned Hours"
msgstr "計画時間数"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "Planned for Today"
msgstr ""

#. module: industry_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_planning
msgid "Planning"
msgstr "計画"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_task__planning_overlap
msgid "Planning Overlap"
msgstr ""

#. module: industry_fsm
#: model:ir.actions.act_window,name:industry_fsm.project_task_action_fsm_planning_groupby_project
msgid "Planning by Project"
msgstr ""

#. module: industry_fsm
#: model:ir.actions.act_window,name:industry_fsm.project_task_action_fsm_planning_groupby_user
msgid "Planning by User"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_product_product_kanban_material
msgid "Price:"
msgstr "価格:"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__priority
msgid "Priority"
msgstr "優先度"

#. module: industry_fsm
#: model:ir.model,name:industry_fsm.model_product_product
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_product_product_kanban_material
msgid "Product"
msgstr "プロダクト"

#. module: industry_fsm
#: model:ir.actions.act_window,name:industry_fsm.product_action_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_settings_product
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
msgid "Products"
msgstr "プロダクト"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_project__allow_material
#: model:ir.model.fields,field_description:industry_fsm.field_project_task__allow_material
msgid "Products on Tasks"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__progress
msgid "Progress"
msgstr "進捗"

#. module: industry_fsm
#: model:ir.model,name:industry_fsm.model_project_project
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__project_id
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "Project"
msgstr "プロジェクト"

#. module: industry_fsm
#: model:ir.actions.act_window,name:industry_fsm.project_project_action_only_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_settings_project
msgid "Projects"
msgstr "プロジェクト"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.res_config_settings_view_form
msgid "Provide worksheet reports to be signed off by customers"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_task__quotation_count
msgid "Quotation Count"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form_fsm_quotation
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form_quotation
msgid "Quotations"
msgstr "見積"

#. module: industry_fsm
#: model:project.task,legend_done:industry_fsm.planning_task_0
#: model:project.task,legend_done:industry_fsm.planning_task_1
#: model:project.task,legend_done:industry_fsm.planning_task_10
#: model:project.task,legend_done:industry_fsm.planning_task_11
#: model:project.task,legend_done:industry_fsm.planning_task_2
#: model:project.task,legend_done:industry_fsm.planning_task_3
#: model:project.task,legend_done:industry_fsm.planning_task_4
#: model:project.task,legend_done:industry_fsm.planning_task_5
#: model:project.task,legend_done:industry_fsm.planning_task_6
#: model:project.task,legend_done:industry_fsm.planning_task_7
#: model:project.task,legend_done:industry_fsm.planning_task_8
#: model:project.task,legend_done:industry_fsm.planning_task_9
#: model:project.task.type,legend_done:industry_fsm.planning_project_stage_0
#: model:project.task.type,legend_done:industry_fsm.planning_project_stage_1
msgid "Ready for Next Stage"
msgstr "次ステージへの準備済"

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid "Record the material you used for the intervention."
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__remaining_hours
msgid "Remaining Hours"
msgstr "残時間"

#. module: industry_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_reporting
msgid "Reporting"
msgstr "レポーティング"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "Responsible"
msgstr "担当者"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
msgid "Resume"
msgstr "レジュメ"

#. module: industry_fsm
#: model:ir.model,name:industry_fsm.model_sale_order
msgid "Sales Order"
msgstr "販売オーダ"

#. module: industry_fsm
#: model:ir.model,name:industry_fsm.model_sale_order_line
msgid "Sales Order Line"
msgstr "販売オーダ明細"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "Search planning"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,help:industry_fsm.field_project_project__timesheet_product_id
msgid ""
"Select a Service product with which you would like to bill your time spent "
"on tasks."
msgstr ""

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid "Select or create a customer."
msgstr ""

#. module: industry_fsm
#: model:product.product,name:industry_fsm.fsm_time_product
#: model:product.template,name:industry_fsm.fsm_time_product_product_template
msgid "Service on Timesheet"
msgstr ""

#. module: industry_fsm
#: model:ir.actions.act_window,name:industry_fsm.res_config_settings_action_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_settings_res_config
msgid "Settings"
msgstr "管理設定"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__stage_id
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "Stage"
msgstr "ステージ"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
msgid "Start"
msgstr "開始"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "Start Date"
msgstr "開始日"

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid "Start recording your time."
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
msgid "Stop"
msgstr "停止"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
msgid "Stop the Timer"
msgstr ""

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid "Stop the timer and save your timesheet."
msgstr ""

#. module: industry_fsm
#: model:ir.model,name:industry_fsm.model_project_task
#: model:ir.model.fields,field_description:industry_fsm.field_sale_order__task_id
msgid "Task"
msgstr "タスク"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_task__fsm_done
msgid "Task Done"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_report_project_task_user_fsm__name
#: model_terms:ir.ui.view,arch_db:industry_fsm.quick_create_task_form_fsm
msgid "Task Title"
msgstr "タスク名"

#. module: industry_fsm
#: model:ir.model.fields,help:industry_fsm.field_sale_order__task_id
msgid "Task from which quotation have been created"
msgstr ""

#. module: industry_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_tasks_kanban
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_calendar_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_list_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "Tasks"
msgstr "タスク"

#. module: industry_fsm
#: model:ir.actions.act_window,name:industry_fsm.project_task_user_action_report_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_reporting_task_analysis
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_user_view_graph
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_user_view_pivot
msgid "Tasks Analysis"
msgstr "タスク分析"

#. module: industry_fsm
#: code:addons/industry_fsm/models/project.py:0
#, python-format
msgid "The FSM task must have a customer set to be sold."
msgstr ""

#. module: industry_fsm
#: model:ir.model.constraint,message:industry_fsm.constraint_project_project_material_imply_billable
msgid "The material can be allowed only when the task can be billed."
msgstr ""

#. module: industry_fsm
#: model:ir.model.constraint,message:industry_fsm.constraint_project_project_timesheet_product_required_if_billable_and_timesheets
msgid ""
"The timesheet product is required when the task can be billed and timesheets"
" are allowed."
msgstr ""

#. module: industry_fsm
#: model_terms:ir.actions.act_window,help:industry_fsm.project_task_action_all_fsm
msgid "There are currently no tasks, click on create to create one"
msgstr ""

#. module: industry_fsm
#: code:addons/industry_fsm/models/project.py:0
#, python-format
msgid "This action is only allowed on FSM project."
msgstr ""

#. module: industry_fsm
#: model_terms:ir.actions.act_window,help:industry_fsm.project_task_user_action_report_fsm
msgid ""
"This report allows you to analyse the performance of your projects and "
"users. You can analyse the quantities of tasks, the hours spent compared to "
"the planned hours, the average number of days to open or close a task, etc."
msgstr ""
"このレポートはあなたのプロジェクトとユーザの成績を分析することができます。タスクの数、計画時間数と消費時間数の比較、タスクを開いて、閉じた平均日数などの分析ができます。"

#. module: industry_fsm
#: code:addons/industry_fsm/models/project.py:0
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
#, python-format
msgid "Time"
msgstr "時間"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.timesheet_view_form
msgid "Time Spent"
msgstr "消費時間"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
msgid "Time recorded on this task"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.timesheet_view_form
msgid "Timesheet"
msgstr "タイムシート"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_project__timesheet_product_id
msgid "Timesheet Product"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "To Do"
msgstr "To Do"

#. module: industry_fsm
#: model:ir.actions.act_window,name:industry_fsm.project_task_action_to_invoice_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_all_tasks_invoice
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "To Invoice"
msgstr "請求対象"

#. module: industry_fsm
#: model:ir.actions.act_window,name:industry_fsm.project_task_action_to_schedule_fsm
#: model:ir.ui.menu,name:industry_fsm.fsm_menu_all_tasks_schedule
msgid "To Schedule"
msgstr ""

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_project_task__fsm_to_invoice
msgid "To invoice"
msgstr "請求書発行"

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_search_fsm
msgid "To schedule"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_view_form_inherit
msgid "Track the material used to complete tasks"
msgstr ""

#. module: industry_fsm
#: model:product.product,uom_name:industry_fsm.field_service_product
#: model:product.product,uom_name:industry_fsm.fsm_time_product
#: model:product.template,uom_name:industry_fsm.field_service_product_product_template
#: model:product.template,uom_name:industry_fsm.fsm_time_product_product_template
msgid "Units"
msgstr "個"

#. module: industry_fsm
#. openerp-web
#: code:addons/industry_fsm/static/src/js/tours/industry_fsm.js:0
#, python-format
msgid "Use the breadcrumbs to <b>go back to your task</b>."
msgstr ""

#. module: industry_fsm
#: model:res.groups,name:industry_fsm.group_fsm_user
msgid "User"
msgstr "ユーザ"

#. module: industry_fsm
#: model:ir.model.fields,field_description:industry_fsm.field_res_config_settings__module_industry_fsm_report
msgid "Worksheet Templates"
msgstr ""

#. module: industry_fsm
#: model_terms:ir.actions.act_window,help:industry_fsm.project_task_action_fsm
msgid "You can create one for yourself by clicking on Create."
msgstr ""

#. module: industry_fsm
#: model_terms:ir.ui.view,arch_db:industry_fsm.project_task_view_form
#: model_terms:ir.ui.view,arch_db:industry_fsm.view_task_form2_inherit
msgid "other tasks"
msgstr ""
