# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* documents_sign
# 
# Translators:
# Martin Trigaux, 2019
# Jeffery CHEN <jeffery9@gmail.com>, 2019
# liAnGjiA <liangjia@qq.com>, 2019
# inspur qiuguodong <qiuguodong@inspur.com>, 2019
# Felix Yuen <fyu@odoo.com>, 2019
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~12.5+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-26 08:05+0000\n"
"PO-Revision-Date: 2019-08-26 09:35+0000\n"
"Last-Translator: Felix Yuen <fyu@odoo.com>, 2019\n"
"Language-Team: Chinese (China) (https://www.transifex.com/odoo/teams/41243/zh_CN/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: zh_CN\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. module: documents_sign
#: model:ir.model,name:documents_sign.model_documents_workflow_rule
msgid ""
"A set of condition and actions which will be available to all attachments "
"matching the conditions"
msgstr "一组适用于符合所有条件附件的状态动作集合"

#. module: documents_sign
#: model:ir.model,name:documents_sign.model_ir_attachment
msgid "Attachment"
msgstr "附件"

#. module: documents_sign
#: model:ir.model.fields,field_description:documents_sign.field_documents_workflow_rule__create_model
msgid "Create"
msgstr "创建"

#. module: documents_sign
#: model:ir.model.fields.selection,name:documents_sign.selection__documents_workflow_rule__create_model__sign_template_new
msgid "Create signature request"
msgstr "创造签字需求"

#. module: documents_sign
#: model:documents.workflow.rule,name:documents_sign.documents_sign_rule_sign_request
#: model:documents.workflow.rule,name:documents_sign.documents_sign_rule_sign_request_finance
msgid "Create signature template(s)"
msgstr "创造（多个）签名模板"

#. module: documents_sign
#: model:ir.model.fields,field_description:documents_sign.field_sign_request__documents_tag_ids
msgid "Document Tags"
msgstr "文档标记"

#. module: documents_sign
#: model:ir.model.fields,field_description:documents_sign.field_sign_request__folder_id
msgid "Document Workspace"
msgstr "文档工作区"

#. module: documents_sign
#: model:ir.model.fields,field_description:documents_sign.field_documents_workflow_rule__has_business_option
msgid "Has Business Option"
msgstr "商业选项"

#. module: documents_sign
#: code:addons/documents_sign/models/workflow.py:0
#, python-format
msgid "New templates"
msgstr "新模板"

#. module: documents_sign
#: model:documents.workflow.rule,name:documents_sign.documents_sign_rule_sign_directly
msgid "Request a signature and sign directly"
msgstr "请求签名并直接签名"

#. module: documents_sign
#: model:ir.model.fields.selection,name:documents_sign.selection__documents_workflow_rule__create_model__sign_template_direct
msgid "Sign directly"
msgstr "直接签名"

#. module: documents_sign
#: model:ir.model,name:documents_sign.model_sign_request
msgid "Signature Request"
msgstr "签字需求"

#. module: documents_sign
#: model:ir.model,name:documents_sign.model_sign_template
msgid "Signature Template"
msgstr "签名模板"

#. module: documents_sign
#: model:ir.model.fields,field_description:documents_sign.field_sign_template__documents_tag_ids
msgid "Signed Document Tags"
msgstr "已签署文档标记"

#. module: documents_sign
#: model:ir.model.fields,field_description:documents_sign.field_sign_template__folder_id
msgid "Signed Document Workspace"
msgstr "已签署文档工作区"
