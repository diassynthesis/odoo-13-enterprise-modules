# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* account_3way_match
# 
# Translators:
# Sergio Zanchetta <primes2h@gmail.com>, 2019
# Martin Trigaux, 2019
# Paolo Valier, 2019
# Léonie Bouchat <lbo@odoo.com>, 2019
# Federico Castellano <castellano.federico@outlook.com>, 2019
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~12.5+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-09 11:58+0000\n"
"PO-Revision-Date: 2019-08-26 09:34+0000\n"
"Last-Translator: Federico Castellano <castellano.federico@outlook.com>, 2019\n"
"Language-Team: Italian (https://www.transifex.com/odoo/teams/41243/it/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: it\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: account_3way_match
#: model:ir.model.fields,help:account_3way_match.field_account_move__release_to_pay_manual
msgid ""
"  * Yes: you should pay the bill, you have received the products\n"
"  * No, you should not pay the bill, you have not received the products\n"
"  * Exception, there is a difference between received and billed quantities\n"
"This status is defined automatically, but you can force it by ticking the 'Force Status' checkbox."
msgstr ""

#. module: account_3way_match
#: model_terms:ir.ui.view,arch_db:account_3way_match.account_invoice_filter_inherit_account_3way_match
msgid "Bills in Exception"
msgstr "Fatture in Eccezione"

#. module: account_3way_match
#: model_terms:ir.ui.view,arch_db:account_3way_match.account_invoice_filter_inherit_account_3way_match
msgid "Bills to Pay"
msgstr "fatture da pagare"

#. module: account_3way_match
#: model:ir.model.fields.selection,name:account_3way_match.selection__account_move__release_to_pay__exception
#: model:ir.model.fields.selection,name:account_3way_match.selection__account_move__release_to_pay_manual__exception
#: model:ir.model.fields.selection,name:account_3way_match.selection__account_move_line__can_be_paid__exception
msgid "Exception"
msgstr "Eccezione"

#. module: account_3way_match
#: model:ir.model.fields,field_description:account_3way_match.field_account_move__force_release_to_pay
msgid "Force Status"
msgstr ""

#. module: account_3way_match
#: model:ir.model.fields,help:account_3way_match.field_account_move__force_release_to_pay
msgid ""
"Indicates whether the 'Should Be Paid' status is defined automatically or "
"manually."
msgstr ""

#. module: account_3way_match
#: model:ir.model,name:account_3way_match.model_account_journal
msgid "Journal"
msgstr "Registro"

#. module: account_3way_match
#: model:ir.model,name:account_3way_match.model_account_move
msgid "Journal Entries"
msgstr "Registrazioni contabili"

#. module: account_3way_match
#: model:ir.model,name:account_3way_match.model_account_move_line
msgid "Journal Item"
msgstr "Movimento contabile"

#. module: account_3way_match
#: model:ir.model.fields.selection,name:account_3way_match.selection__account_move__release_to_pay__no
#: model:ir.model.fields.selection,name:account_3way_match.selection__account_move__release_to_pay_manual__no
#: model:ir.model.fields.selection,name:account_3way_match.selection__account_move_line__can_be_paid__no
msgid "No"
msgstr "N°"

#. module: account_3way_match
#: model:ir.model.fields,field_description:account_3way_match.field_account_move__release_to_pay
msgid "Release To Pay"
msgstr ""

#. module: account_3way_match
#: model:ir.model.fields,field_description:account_3way_match.field_account_move_line__can_be_paid
msgid "Release to Pay"
msgstr "Consegna da pagare"

#. module: account_3way_match
#: model:ir.model.fields,field_description:account_3way_match.field_account_move__release_to_pay_manual
msgid "Should Be Paid"
msgstr ""

#. module: account_3way_match
#: model:ir.model.fields,help:account_3way_match.field_account_move__release_to_pay
msgid ""
"This field can take the following values :\n"
"  * Yes: you should pay the bill, you have received the products\n"
"  * No, you should not pay the bill, you have not received the products\n"
"  * Exception, there is a difference between received and billed quantities\n"
"This status is defined automatically, but you can force it by ticking the 'Force Status' checkbox."
msgstr ""
"Questo campo può avere i seguenti valori:\n"
"  * Sì, devi pagare la fattura poiché hai ricevuto i prodotti\n"
"  * No, non devi pagare la fattura poiché non hai ricevuto i prodotti\n"
"  * Eccezione: la quantità ricevuta e la quantità fatturata sono diverse.\n"
"Questo stato viene impostato automaticamente ma è possibile forzarlo selezionando la casella \"Forza lo Stato\"."

#. module: account_3way_match
#: model:ir.model.fields.selection,name:account_3way_match.selection__account_move__release_to_pay__yes
#: model:ir.model.fields.selection,name:account_3way_match.selection__account_move__release_to_pay_manual__yes
#: model:ir.model.fields.selection,name:account_3way_match.selection__account_move_line__can_be_paid__yes
msgid "Yes"
msgstr "Sì"
